import pandas
import os
import numpy as np


path = "B:\\MyKeystroke\\keystroke.csv"
data = pandas.read_csv(path)
p_path = os.getcwd()
for i, subjects in data.groupby("subject"):
    os.mkdir('{}'.format(i))
    os.chdir('{}'.format(i))
    for i, sessionIndex in subjects.groupby("sessionIndex"):
        sessionIndex.to_csv('{}.csv'.format(i), index_label=False, index=False)
    os.chdir(p_path)
